package com.github.resultanalyser.mapper;

public abstract class AbstractColumnMapper<T> implements ColumnMapper<T> {
    final int columnIndex;
    private final String columnName;

    AbstractColumnMapper(int columnIndex, String columnName) {
        this.columnIndex = columnIndex;
        this.columnName = columnName;
    }

    @Override
    public String getColumnName() {
        return columnName;
    }
}
