package com.github.resultanalyser.mapper;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.List;
import java.util.Locale;

public class NumberColumnMapper extends AbstractColumnMapper<Double> {
    private static final NumberFormat format = NumberFormat.getInstance(Locale.GERMAN);

    public NumberColumnMapper(int columnIndex, String columnName) {
        super(columnIndex, columnName);
    }

    @Override
    public Double map(List<String> columns) throws MappingException {
        try {
            return format.parse(columns.get(columnIndex).trim()).doubleValue();
        } catch (ParseException e) {
            throw new MappingException(e);
        }
    }
}
