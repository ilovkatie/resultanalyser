package com.github.resultanalyser.mapper;

public class MappingException extends Exception {

    public MappingException(Exception e) {
        super(e);
    }
}
