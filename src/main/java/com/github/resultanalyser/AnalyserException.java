package com.github.resultanalyser;

class AnalyserException extends RuntimeException {
    AnalyserException(String msg) {
        super(msg);
    }
}
