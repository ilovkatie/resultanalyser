package com.github.resultanalyser.printer;

abstract class BaseComponentStatisticsPrinter implements ComponentStatisticsPrinter {
    final ReportElements reportElements;
    final ReportNumberFormat reportNumberFormat;

    BaseComponentStatisticsPrinter(ReportElements reportElements, ReportNumberFormat reportNumberFormat) {
        this.reportElements = reportElements;
        this.reportNumberFormat = reportNumberFormat;
    }
}
