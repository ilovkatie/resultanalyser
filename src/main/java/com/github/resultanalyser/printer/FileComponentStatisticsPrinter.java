package com.github.resultanalyser.printer;

import com.github.resultanalyser.ComponentStatistics;

public class FileComponentStatisticsPrinter extends BaseComponentStatisticsPrinter {
    private final String fileName;

    FileComponentStatisticsPrinter(ReportElements reportElements, ReportNumberFormat reportNumberFormat, String fileName) {
        super(reportElements, reportNumberFormat);
        this.fileName = fileName;
    }

    @Override
    public void print(ComponentStatistics componentStatistics) {
        //TODO: save to file
    }
}
