package com.github.resultanalyser;

import org.apache.commons.math3.stat.Frequency;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

public class ComponentStatistics {
    private final String name;
    private final DescriptiveStatistics descriptiveStatistics;
    private final Frequency frequency;

    ComponentStatistics(String name, DescriptiveStatistics descriptiveStatistics, Frequency frequency) {
        this.name = name;
        this.descriptiveStatistics = descriptiveStatistics;
        this.frequency = frequency;
    }

    public String getName() {
        return name;
    }

    public Map<Double, Double> getPercentilesFor(List<Double> values) {
        return values.stream()
                .collect(Collectors
                        .toMap(Function.identity(),
                                descriptiveStatistics::getPercentile,
                                (v1, v2) -> {
                                    throw new AnalyserException(String.format("Duplicate key for values %s and %s", v1, v2));
                                },
                                TreeMap::new));
    }

    //TODO: clean up this ugly method...
    public Map<String, Double> getPercentageDistributionForRange(double max, double step, DecimalFormat df) {
        List<String> steps = DoubleStream.iterate(0.0, d -> d + step)
                .limit((long) Math.ceil((max - 0.0) / step) + 1)
                .boxed()
                .sorted()
                .map(df::format)
                .collect(Collectors.toList());

        List<Double> frequencies = steps.stream().sorted().map(source -> {
            try {
                return df.parse(source).doubleValue();
            } catch (ParseException e) {
                throw new AnalyserException("Cannot parse to double " + e.getMessage());
            }
        }).map(frequency::getCumPct).collect(Collectors.toList());

        Map<String, Double> percentageDistribution = new TreeMap<>();
        for (int i = 0; i < steps.size(); i++) {
            double lower;
            double upper;

            String upperLabel = "";
            if (i == steps.size() - 1) {
                upper = 1.0;
            } else {
                upper = frequencies.get(i + 1);
                upperLabel = steps.get(i + 1);
            }

            if (i == 0) {
                lower = 0.0;
            } else {
                lower = frequencies.get(i);
            }
            String lowerLabel = steps.get(i);

            percentageDistribution.put(String.format("%s - %s", lowerLabel, upperLabel), upper - lower);
        }

        return percentageDistribution;
    }

    public DescriptiveStatistics getDescriptiveStatistics() {
        return descriptiveStatistics;
    }

    public Frequency getFrequency() {
        return frequency;
    }
}